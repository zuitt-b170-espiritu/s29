// this require("express") allows devs to load/import express package that will be used for the application
// let express = require("express");


// express() - allows devs to create an application using express
// const app = express(); //this code creates an express application and stores it inside the "app" variable: thus, "app" is the server already


// const port = 3000;



// Express has methods corresponding to each http methods (get, post, put, delete, etc)
// this "/" route expects to receive a get request at its endpoint (http://localhost3000/)
// app.get("/",(req, res)=>{
// 	// res.send - allows sending of messages as responses to the client
// 	res.send("Hello World")
// })

// app.listen(port, ()=>console.log(`Server is running at port ${port}`));


/*
	miniactivity
		create a "/" route that will receive a GET request and will send a message to the client
			message = "hello from the hello endpoint"
			send the screenshot of your output in the batch google chat

*/

let express = require("express");

const app = express(); 

const port = 3000;


// use function lets the middleware to do common services and capabilities to the application
	// 
app.use(express.json()); //lets the app to read json data
app.use(express.urlencoded({extended:true})); //allows the app to receive data from the forms
	// not using these will result to our

/*app.get("/hello",(req, res)=>{
	res.send("Hello from the /hello endpoint")
})


app.post("/hello",(req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})


app.listen(port, ()=>console.log(`Server is running at port ${port}`));*/




/*s29 Activity*/

app.get("/home",(req, res)=>{
	res.send("Hello! Welcome to my vlog")
})


app.get("/users",(req,res)=>{
	res.send(`Hello there ${req.body.userName} ${req.body.userPassword}`)
})


app.delete("/delete-user",(req,res)=>{
	res.send(`You deleted user ${req.body.userName} ${req.body.userPassword}`)
})


app.listen(port, ()=>console.log(`Server is running at port ${port}`));